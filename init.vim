scriptencoding utf-8

let g:wire = get(g:, 'wire', {})
let g:wire.base = $HOME.'/.wire.vim'
let g:wire.core = $HOME.'/.wire.vim/core'
let g:wire.crate_root = $HOME . '/.wire.vim/crates'
let g:wire.version = '2018.12.1-alpha'

function! s:check_environment() abort
  " check platform
  let g:wire.os = {}
  let g:wire.os.mac = has('macunix')
  let g:wire.os.linux = has('unix') && !has('macunix') &&
      \ !has('win32unix')
  let g:wire.os.win = has('win32')

  " check vim version
  let g:wire.nvim = has('nvim') && exists('*jobwait') && !g:wire.os.win
  let g:wire.vim8 = exists('*job_start')
  let g:wire.timer = exists('*timer_start')
  let g:wire.gui = exists('gui_running')
  let g:wire.tmux = !empty($TMUX)
endfunction

execute 'set runtimepath+=' . g:wire.core

call s:check_environment()

call wire#bootstrap()

