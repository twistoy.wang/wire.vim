# wire.vim

-------------

Simple vim/neovim configuration

## Usage

```
~# git clone https://gitlab.com/twistoy.wang/wire.vim.git
~# mkdir -p ~/.config/nvim
~# ln -s ~/wire.vim/init.vim ~/.config/nvim/init.vim
~# ln -s ~/wire.vim ~/.wire.vim
```

Open neovim to install required plugins.

-------
inspired by:

- [space-vim](https://github.com/liuchengxu/space-vim)


