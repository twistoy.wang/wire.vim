scriptencoding utf-8

" global settings {{{
syntax enable
let $LANG = 'en_US'

" close vi features
if !g:wire.nvim
  set nocompatible
  set backspace=2
endif

" disable <Left> <Right> <Up> and <Down>
map <Left> <Nop>
map <Right> <Nop>
map <Up> <Nop>
map <Down> <Nop>
"}}}

" default theme setting {{{
silent! color wombat256mod
" }}}

set title
set ttyfast " Improves smoothness of redrawing

" Don't redraw while executing macros (good performance config)
set lazyredraw

" No bells
set noerrorbells
set novisualbell
set t_vb=

" show trailing white-space {{{
hi ExtraWhitespace guifg=#FF2626 gui=underline ctermfg=124 cterm=underline
match ExtraWhitespace /\s\+$/
" }}}

let s:version_str = 'nvim ' . wire#utils#get_nvim_version()

" startify settings {{{
let g:startify_custom_header = [ ' [ wire.vim ' . g:wire.version . ' @' . s:version_str . ' ]' ]
let g:startify_list_order = [
      \ [' Recent Files:'],
      \ 'files',
      \ [' Project:'],
      \ 'dir',
      \ [' Sessions:'],
      \ 'sessions',
      \ [' Bookmarks:'],
      \ 'bookmarks',
      \ [' Commands:'],
      \ 'commands',
      \ ]
let g:startify_change_to_vcs_root = 1
" }}}

" relativenumbers {{{
set number relativenumber
augroup numberToggle
  autocmd!
  autocmd BufEnter, FocusGained, InsertLeave * set relativenumber
  autocmd BufLeave, FocusLost, InsertEnter   * set norelativenumber
augroup END
" }}}

" vim-which-key {{{
nnoremap <silent> <leader>      :<c-u>WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey ','<CR>
call wire#default#keybindings#trigger()
" }}}

" rainbow settings {{{
let g:rainbow_active = 1
autocmd FileType c,cpp,java,php autocmd BufWritePre <buffer> %s/\s\+$//e
command! -bang -nargs=* -complete=file Make AsyncRun -program=make @ <args>
" }}}

let g:fzf_colors = g:wire#default#fzf#colors

" scrooloose/nerdtree {{{
let g:NERDTreeShowHidden = 1
let g:NERDTREEAutoDeleteBuffer = 1
let g:NERDTreeDirArrowCollapsible = "~"
let g:NERDTreeDirArrowExpandable = "\u276f"

augroup loadNerdTree
  autocmd!
  autocmd VimEnter * silent! autocmd! FileExplorer
  autocmd BufEnter, BufNew *
      \ if isdirectory(expand('<amatch>'))
      \   call plug#load('nerdtree')
      \   call nerdtree#checkForBrowser(expand('<amatch>'))
      \ endif
augroup END
" }}}

set tabstop=2 shiftwidth=2 autoindent expandtab

" disable auto comment
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" disable surround default keybindings {{{
let g:surround_no_mappings = 0
let g:surround_no_insert_mappings = 0
" }}}

" auto-move quickfix botright {{{
autocmd FileType qf wincmd J
" "}}}

" vim-airline {{{

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'

" }}}
