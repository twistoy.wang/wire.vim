UsePlug 'liuchengxu/vim-which-key'
UsePlug 'skywind3000/asyncrun.vim'
UsePlug 'mhinz/vim-startify'

UsePlug 'scrooloose/nerdtree', { 'on': ['NERDTreeToggle', 'NERDTreeFind'] }
UsePlug 'Xuyuanp/nerdtree-git-plugin', { 'on': ['NERDTreeToggle', 'NERDTreeFind'] }

UsePlug 'Yggdroot/LeaderF', { 'do': './install.sh' }

UsePlug 'vim-airline/vim-airline'
UsePlug 'tpope/vim-surround'
UsePlug 'tpope/vim-fugitive'

UsePlug 'easymotion/vim-easymotion'

UsePlug 'liuchengxu/space-vim-dark'
UsePlug 'tpope/vim-vividchalk'
UsePlug 'altercation/vim-colors-solarized'
UsePlug 'vim-scripts/wombat256.vim'

call timer_start(200, 'wire#defer#whichkey')
call timer_start(700, 'wire#defer#leaderf')
