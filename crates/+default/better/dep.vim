UsePlug 'dominikduda/vim_current_word'

UsePlug 'matze/vim-move'

UsePlug 'godlygeek/tabular',        { 'on': 'Tabularize' }
UsePlug 'junegunn/vim-easy-align',  { 'on': [ '<Plug>(EasyAlign)', 'EasyAlign' ] }

UsePlug 'scrooloose/nerdcommenter'
UsePlug 'sheerun/vim-polyglot'

UsePlug 'Yggdroot/indentLine'
UsePlug 'tpope/vim-repeat'

UsePlug 'terryma/vim-multiple-cursors'

UsePlug 'tpope/vim-speeddating'

" Open file in given line
UsePlug 'bogado/file-line'

UsePlug 'Raimondi/delimitMate'

" Denite support
UsePlug 'Shougo/denite.nvim'

UsePlug 'andymass/vim-matchup'
UsePlug 'tenfyzhong/axring.vim'

