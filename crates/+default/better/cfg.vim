scriptencoding utf-8

" vim_current_word {{{

" Twins of word under cursor:
let g:vim_current_word#highlight_twins = 1
" The word under cursor:
let g:vim_current_word#highlight_current_word = 1
let g:vim_current_word#highlight_only_in_focused_window = 1
let g:vim_current_word#enabled = 1

" }}}

" vim-mode {{{

let g:move_key_modifier = 'C'

" }}}

xmap <leader>ta <Plug>(EasyAlign)
nmap <leader>ta <Plug>(EasyAlign)
call wire#vim#keybinding#reg_leader('ta', 'easy-align')


" scrooloose/nerdcommenter {{{

let g:NERDSpaceDelims = 1

let g:NERDCompactSexyComs = 1

let g:NERDTrimTrailingWhitespace = 1

" }}}

" better settings {{{

" Show ruler at column 80
set colorcolumn=80

set tabstop=2
set shiftwidth=2
set expandtab
set smartindent

" soft wrap at 80
" set columns=80
" autocmd VimResized * if (&columns > 80) | set columns=80 | endif
set wrap
set linebreak
set showbreak='↳'

" }}}

" better google coding style {{{

function! GoogleCppIndent() abort
  let l:cline_num = line('.')
  let l:orig_indent = cindent(l:cline_num)

  if l:orig_indent == 0 | return 0 | endif

  let l:pline_num = prevnonblank(l:cline_num - 1)
  let l:pline = getline(l:pline_num)
  let l:pline_indent = indent(l:pline_num)

  " if prev line is tempalte, keep the indent
  if l:pline =~# '^\s*template' | return l:pline_indent | endif

  if l:orig_indent != &shiftwidth | return l:orig_indent | endif

  let l:in_comment = 0
  while l:pline_num > -1
    let l:pline = getline(l:pline_num)
    let l:pline_indent = indent(l:pline_num)

    if l:in_comment == 0 && l:pline =~ '^.\{-}\(/\*.\{-}\)\@<!\*/'
      let l:in_comment = 1
    elseif l:in_comment == 1
      if l:pline =~ '/\*\(.\{-}\*/\)\@!'
        let l:in_comment = 0
      endif
    elseif l:pline_indent == 0
      if l:pline !~# '\(#define\)\|\(^\s*//\)\|\(^\s*{\)'
        if l:pline =~# '^\s*namespace.*'
          return 0
        else
          return l:orig_indent
        endif
      elseif l:pline =~# '\\$'
        return l:orig_indent
      endif
    else
      return l:orig_indent
    endif

    let l:pline_num = prevnonblank(l:pline_num - 1)
  endwhile

  return l:orig_indent
endfunction


" }}}

if g:wire.nvim
  set inccommand=split
endif

set scrolloff=5


" denite settings {{{

call denite#custom#map('insert', '<C-P>', '<denite:move_to_previous_line>','noremap')
call denite#custom#map('insert', '<C-N>', '<denite:move_to_next_line>','noremap')
call denite#custom#map('insert', '<Up>', '<denite:move_to_previous_line>','noremap')
call denite#custom#map('insert', '<Down>', '<denite:move_to_next_line>','noremap')

" }}}

" axring settings {{{

let g:axring_rings = [
      \ ['true', 'false'],
      \ ['Debug', 'Info', 'Warn', 'Error', 'Fatal'], 
      \ ]

" }}}
