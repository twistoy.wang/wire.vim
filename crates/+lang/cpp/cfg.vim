function! s:keybind_cpp() abort
  nnoremap <silent> <leader>fa :FSHere<CR>
  call wire#vim#keybinding#reg_leader('fa', 'switch-here')
  nnoremap <silent> <leader>fc :ClangFormat<CR>
  call wire#vim#keybinding#reg_leader('fc', 'clang-format')
  nnoremap <silent> <leader>fv :FSSplitRight<CR>
  call wire#vim#keybinding#reg_leader('fs', 'switch-split-right')
endfunction

augroup crateCpp
  autocmd!
  autocmd FileType c,cpp call s:keybind_cpp()
augroup END
