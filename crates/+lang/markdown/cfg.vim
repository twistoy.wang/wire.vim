" disable default key mappings
let g:vim_markdown_no_default_key_mappings = 1

" auto start markdown-preview
let g:mkdp_auto_start = 1
let g:mkdp_open_to_the_world = 1
let g:mkdp_echo_preview_url = 1

" conceal settings
set conceallevel=0
let g:vim_markdown_conceal = 0
