function! s:make_docker_command(target) abort
  if !exists('g:lmake_build_image')
    echo "[lmake] BuildEnv image is required."
    return ""
  endif
  let l:cmd = 'docker run -i --rm '
  if exists('g:lmake_uid')
    let l:cmd = l:cmd . '--user=' . g:lmake_uid . ' '
  endif
  let l:root_path = lmake#get_build_root()

  let l:cmd = l:cmd . '-v ' . l:root_path . ':/data ' . g:lmake_build_image
  let l:cmd = l:cmd . ' make ' . a:target . ' -j8'

  return l:cmd
endfunction

" Generate makefile
function! s:generate_makefile() abort
  if !lmake#is_lmake_project()
    echom "[wire.vim] This is not a file in lmake project."
    return
  endif

  let root_path = lmake#get_build_root()
  let build_file = lmake#get_current_rule_file()
  execute 'AsyncRun -cwd=' . root_path . ' ./gen_makefile.sh ' . build_file . ' -m32'
  execute 'botright copen'
endfunction

nnoremap <silent><leader>mg :call <SID>generate_makefile()<CR>
call wire#vim#keybinding#reg_leader('mg', 'gen-makefile(lm)')

" Do make
function! s:do_make(target) abort
  if !lmake#is_lmake_project()
    echom "[wire.vim] This is not a file in lmake project."
    return
  endif

  let root_path = lmake#get_build_root()
  if exists('g:lmake_in_docker') && g:lmake_in_docker
    execute 'AsyncRun ' . s:make_docker_command(a:target)
  else
    execute 'AsyncRun -cwd=' . root_path . ' make ' . a:target . ' -j8'
  endif
  execute 'botright copen'
endfunction

function! s:files_from_root() abort
  if !lmake#is_lmake_project()
    echom "[wire.vim] This is not a file in lmake project."
    return
  endif

  let l:root_path = lmake#get_build_root()
  execute 'Leaderf file ' . l:root_path
endfunction

nnoremap <silent><leader>mm :call <SID>do_make('release')<CR>
nnoremap <silent><leader>mt :call <SID>do_make('test')<CR>
nnoremap <silent><leader>fr :call <SID>files_from_root()<CR>

call wire#vim#keybinding#reg_leader('mm', 'make-release(lm)')
call wire#vim#keybinding#reg_leader('mt', 'make-test(lm)')
call wire#vim#keybinding#reg_leader('fr', 'edit(<LMAKE_ROOT>)')
