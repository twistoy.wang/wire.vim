""" settings for 'deoplete' {

let g:deoplete#enable_at_startup = 1
let g:deoplete#auto_complete_delay = 0

""" }

""" settings for 'Shougo/echodoc.vim' {

let g:echodoc#enable_at_startup = 1
set noshowmode

""" }

let g:min_pattern_length = 2
let g:LanguageClient_selectionUI = 'quickfix'
let g:LanguageClient_loadSettings = 1
let g:LanguageClient_settingsPath = $HOME . '/.config/nvim/settings.json'
let g:LanguageClient_serverCommands = {
    \ 'cpp': ['ccls', '--log-file=/tmp/cq.log'],
    \ 'c': ['ccls', '--log-file=/tmp/cq.log'],
    \ }

set signcolumn=yes
call deoplete#custom#source('LanguageClient', 'min_pattern_length', 2)

nnoremap <silent> <leader>gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <silent> <leader>gr :call LanguageClient_textDocument_references()<CR>
nnoremap <silent> <leader>gi :call LanguageClient_textDocument_implementation()<CR>
nnoremap <silent> <leader>gR :call LanguageClient_textDocument_rename()<CR>
nnoremap <silent> <leader>gs :call LanguageClient_textDocument_documentSymbol()<CR>
nnoremap <silent> <leader>gS :call LanguageClient_workspace_symbol()<CR>
nnoremap <silent> <leader>gt :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> <leader>gf :call LanguageClient_textDocument_codeAction()<CR>
nnoremap <silent> <leader>gc :call LanguageClient_textDocument_completion()<CR>

autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"

set cmdheight=2

