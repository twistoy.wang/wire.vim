nmap <silent> <leader>ga <Plug>(coc-codeaction)
call wire#vim#keybinding#reg_leader('ga', 'lsp-codeaction')
nmap <silent> <leader>gd <Plug>(coc-definition)
call wire#vim#keybinding#reg_leader('gd', 'lsp-goto-definition')
nmap <silent> <leader>gy <Plug>(coc-type-definition)
call wire#vim#keybinding#reg_leader('gy', 'lsp-goto-type-definition')
nmap <silent> <leader>gi <Plug>(coc-implementation)
call wire#vim#keybinding#reg_leader('gi', 'lsp-goto-implementation')
nmap <silent> <leader>gr <Plug>(coc-references)
call wire#vim#keybinding#reg_leader('gr', 'lsp-goto-references')
nmap <silent> <leader>gp :call CocActionAsync('doHover')<CR>
call wire#vim#keybinding#reg_leader('gp', 'lsp-peek')

function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

nnoremap <silent> <leader>gk :call <SID>show_documentation()<CR>
call wire#vim#keybinding#reg_leader('gk', 'lsp-show-doc')

if exists('g:stalker_coc_show_signature_help') && g:stalker_coc_show_signature_help
  autocmd CursorHoldI,CursorMovedI * silent! call CocAction('showSignatureHelp')
endif

" autocmd CursorHold * silent call CocActionAsync('highlight')

" Rename
nmap <silent> <leader>gR <Plug>(coc-rename)
call wire#vim#keybinding#reg_leader('gR', 'lsp-rename')

" Close preview window when completion is done
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

let g:UltiSnipsEditSplit = "vertical"
let g:UltiSnipsExpandTrigger="<c-f>"
let g:UltiSnipsJumpForwardTrigger="<c-f>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
let g:coc_snippet_next = "<C-f>"
let g:coc_snippet_prev = "<C-b>"

nmap <silent> [c <plug>(coc-diagnostic-prev)
nmap <silent> ]c <plug>(coc-diagnostic-prev)

inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<cr>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

set cmdheight=2
set signcolumn=yes
set hidden

let g:airline_section_error = '%{airline#util#wrap(airline#extensions#coc#get_error(),0)}'
let g:airline_section_warning = '%{airline#util#wrap(airline#extensions#coc#get_warning(),0)}'

