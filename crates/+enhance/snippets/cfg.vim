set runtimepath+=$HOME/.wire.vim/crates/+enhance/snippets/snippets

let g:UltiSnipsJumpForwardTrigger="<c-f>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
let g:UltiSnipsExpandTrigger="<c-space>"
