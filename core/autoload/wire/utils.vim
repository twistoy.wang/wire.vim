let s:logger = wire#vim#log#get_logger('wire.utils')


function! wire#utils#check_vim_plugin() abort
  let l:vim_plug = g:wire.nvim ?
      \ '~/.local/share/nvim/site/autoload/plug.vim':
      \ '~/.vim/autoload/plug.vim'

  if empty(glob(l:vim_plug))
    call wire#vim#plug#download_vim_plug(l:vim_plug)
  endif
endfunction

let wire#utils#TYPE = {
  \ 'string':  type(''),
  \ 'list':    type([]),
  \ 'dict':    type({}),
  \ 'funcref': type(function('call'))
  \ }

function! wire#utils#source(file) abort
  if filereadable(expand(a:file))
    execute 'source ' . fnameescape(a:file)
  else
    call s:logger.warn('Try to source file ' . a:file . ', but failed.')
  endif
endfunction

function! wire#utils#get_nvim_version() abort
  redir => l:s
  silent! version
  redir END
  return matchstr(l:s, 'NVIM v\zs[^\n]*')
endfunction
