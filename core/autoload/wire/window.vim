function! wire#window#mark() abort
  let g:wire_maked_win_num = winnr()
endfunction

function! wire#window#swap() abort
  let cur_num = winnr()
  let cur_buf = bufnr("%")
  exe g:wire_maked_win_num . 'wincmd w'
  let marked_buf = bufnr('%')
  exe 'hide buf' cur_buf
  exe cur_num . 'wincmd w'
  exe 'hide buf' marked_buf
endfunction

function! s:find_quickfix_window() abort
  for i in range(1, bufnr('$'))
    let bnum = winbufnr(i)
    if getbufvar(bnum, '&buftype') == 'quickfix'
      return 1
    endif
  endfor
  return 0
endfunction

function! wire#window#toggle_quickfix() abort
  let have_quickfix = s:find_quickfix_window()
  if have_quickfix
    cclose
  else
    copen
  endif
endfunction
