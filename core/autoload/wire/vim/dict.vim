function! wire#vim#dict#merge_dict(expr1, expr2)
  if type(a:expr1) != type({}) || type(a:expr2) != type({})
    throw 'Error'
  endif

  for key in keys(a:expr2)
    if !has_key(a:expr1, key)
      let a:expr1[key] = a:expr2[key]
      continue
    endif

    let exp1 = a:expr1[key]
    let exp2 = a:expr2[key]

    if type(exp1) == type(exp2) && type(exp1) == type([])
      call extend(exp1, exp2)
      continue
    endif

    if type(exp1) == type(exp2) && type(exp1) == type({})
      call wire#vim#dict#merge_dict(exp1, exp2)
      continue
    endif

    " final override
    let a:expr1[key] = a:expr2[key]
  endfor
endfunction

function! wire#vim#dict#gen_dict(...) abort
  if a:0 == 1
    " final
    return a:1
  endif
  return { a:1: call('wire#vim#dict#gen_dict', a:000[1:]) }
endfunction