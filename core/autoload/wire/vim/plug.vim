let s:logger = wire#vim#log#get_logger('wire.plug')

function! wire#vim#plug#install_all() "{{{
  if !exists('g:wire_plug_root')
    let g:wire_plug_root = '~/.local/share/nvim/plugged'
  endif

  " plug from crates
  for l:crate in g:wire.loaded_crates
    if has_key(g:available_crates, l:crate)
      let l:dep_file = g:available_crates[l:crate].dir . '/dep.vim'
      call wire#utils#source(l:dep_file)
    else
      call s:logger.warn('Crate "' . l:crate . '" not found.')
    endif
  endfor

  call plug#begin(g:wire_plug_root)

  for l:plug in g:wire.plugins
    call plug#(l:plug, get(g:wire.plugin_options, l:plug, ''))
  endfor

  call plug#end()

  augroup checkPlug
    autocmd!
    autocmd VimEnter * call wire#vim#plug#install_missing(1)
  augroup END
endfunction "}}}

function! wire#vim#plug#download_vim_plug(plug_path) abort "{{{
  echo 'Downloading vim-plug...'
  execute '!curl -fLo' a:plug_path '--create-dirs'
        \   'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
endfunction "}}}

function! wire#vim#plug#plug_directory(plug) "{{{
  let l:name = split(a:plug, '/')[-1]
  let l:name = get(get(g:wire.plugin_options, a:plug, {}), 'as', l:name)

  return expand(g:wire_plug_root) . '/' . l:name
endfunction "}}}

function! wire#vim#plug#defer_install_missing(timer) abort "{{{
  call wire#vim#plug#install_missing(1)
endfunction  "}}}

function! wire#vim#plug#install_missing(...) abort "{{{
  if !exists('g:wire_plug_root')
    let g:wire_plug_root = '~/.local/share/nvim/plugged'
  endif

  let l:plugin_dicts = []
  for l:p in g:wire.plugins
    let l:plugin_dicts = add(l:plugin_dicts, {
          \ 'dir': wire#vim#plug#plug_directory(l:p),
          \ 'name': l:p})
  endfor

  let l:msg = '[wire.vim] Need to install the missing plugins: '
  let missing = filter(l:plugin_dicts, '!isdirectory(v:val.dir)')

  if len(missing)
    let plugs = map(missing, 'v:val.name')
    let l:msg .= string(plugs)
    if a:0 == 1
      if s:ask(l:msg)
        call s:logger.info('Installing missing plugins accepted.')
        silent PlugInstall --sync | q
      else
        call s:logger.info('Installing missing plugins rejected.')
      endif
    else
      call s:logger.info('Installing missing plugins without confirm.')
      echom l:msg
      PlugInstall --sync | q
    endif
  endif
endfunction "}}}

function! s:ask(message) abort "{{{
  call inputsave()
  echohl WarningMsg
  let answer = input(a:message.(' (y/N): '))
  echohl None
  call inputrestore()
  echo '\r'
  return (answer =~? '^y') ? 1: 0
endfunction "}}}

