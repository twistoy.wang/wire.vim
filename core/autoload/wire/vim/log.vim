let g:log_file_path = get(g:, 'wire_log_file_path', $HOME . '/wire.vim.log')

let s:loggers = {}

let s:logger_base = {
      \ 'name': '',
      \ 'debug': function('wire#vim#log#log_debug'),
      \ 'info': function('wire#vim#log#log_info'),
      \ 'warn': function('wire#vim#log#log_warn'),
      \ 'error': function('wire#vim#log#log_error'),
      \ }
let s:levels = {
      \ 1: 'Debug',
      \ 2: 'Info ',
      \ 3: 'Warn ',
      \ 4: 'Error'
      \ }

function! wire#vim#log#get_logger(name) abort
  if has_key(s:loggers, a:name) " {
    return s:loggers[a:name]
  endif " }
  let s:loggers[a:name] = deepcopy(s:logger_base)
  let s:loggers[a:name].name = a:name
  return s:loggers[a:name]
endfunction

function! wire#vim#log#log_debug(msg) dict
  call s:log_impl(a:msg, 1, self.name)
endfunction

function! wire#vim#log#log_info(msg) dict
  call s:log_impl(a:msg, 2, self.name)
endfunction

function! wire#vim#log#log_warn(msg) dict
  call s:log_impl(a:msg, 3, self.name)
endfunction

function! wire#vim#log#log_error(msg) dict
  call s:log_impl(a:msg, 4, self.name)
endfunction

function! s:log_impl(msg, tag, name) abort
  let l:lines = []

  call add(l:lines, s:parse_tag(a:tag))
  call add(l:lines, a:name)
  call add(l:lines, s:parse_time('%c'))
  call add(l:lines, s:parse_msg(a:msg))

  let l:write_line = join(l:lines, ' ')
  if g:wire.nvim
    call s:write_nvim(l:write_line)
  else
    call s:write_vim(l:write_line)
  endif
endfunction

function! s:write_vim(line) abort
lua << EOL
  fout = io.open(vim.eval('g:log_file_path'), 'a')
  if fout == nil then
    return
  end
  fout:write(vim.eval('a:line') .. '\n')
  fout:close()
EOL
endfunction

function! s:write_nvim(line) abort
lua << EOL
  fout = io.open(vim.api.nvim_eval('g:log_file_path'), 'a')
  if fout == nil then
    return
  end
  fout:write(vim.api.nvim_eval('a:line') .. '\n')
  fout:close()
EOL
endfunction

function! s:parse_time(format)
  let l:time = strftime(a:format)
  return s:sandwitch_brackets(l:time)
endfunction

function! s:parse_tag(tag)
  return has_key(s:levels, a:tag) ?
        \ s:sandwitch_brackets(s:levels[a:tag]) : ''
endfunction

function! s:parse_msg(msg)
  let l:msg = substitute(a:msg, '^\"', '', 'g')
  return substitute(l:msg, '\"$', '', 'g')
endfunction

function! s:sandwitch_brackets(val)
  return '[' . a:val . ']'
endfunction
