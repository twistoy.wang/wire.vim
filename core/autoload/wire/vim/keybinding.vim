let g:bind_map = get(g:, 'bind_map', {})
let g:local_bind_map = get(g:, 'local_bind_map', {})

function! wire#vim#keybinding#reg_leader_name(key, name) abort
  let keys = split(a:key, '\zs')

  let name_dict = call('wire#vim#dict#gen_dict', keys + ['name', a:name])
  call wire#vim#dict#merge_dict(g:bind_map, name_dict)
endfunction

function! wire#vim#keybinding#reg_leader(key, key_obj)
  let keys = split(a:key, '\zs')

  let name_dict = call('wire#vim#dict#gen_dict', keys + [a:key_obj])
  call wire#vim#dict#merge_dict(g:bind_map, name_dict)
endfunction

function! wire#vim#keybinding#reg_local_leader_name(key, name) abort
  let keys = split(a:key, '\zs')

  let name_dict = call('wire#vim#dict#gen_dict', keys + ['name', a:name])
  call wire#vim#dict#merge_dict(g:local_bind_map, name_dict)
endfunction

function! wire#vim#keybinding#reg_local_leader(key, key_obj)
  let keys = split(a:key, '\zs')

  let name_dict = call('wire#vim#dict#gen_dict', keys + [a:key_obj])
  call wire#vim#dict#merge_dict(g:local_bind_map, name_dict)
endfunction

function! wire#vim#keybinding#build() abort
  let g:leader_map = deepcopy(g:bind_map)
  let g:local_leader_map = deepcopy(g:local_bind_map)

  call which_key#register('<Space>', 'g:leader_map')
  call which_key#register(',', 'g:local_leader_map')
endfunction