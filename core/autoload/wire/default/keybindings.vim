function s:_reg(...)
  call call('wire#vim#keybinding#reg_leader', a:000)
endfunction

function s:_regn(...)
  call call('wire#vim#keybinding#reg_leader_name', a:000)
endfunction

let g:wire#default#keybindings#triggered = 0
function! wire#default#keybindings#trigger() abort
  if g:wire#default#keybindings#triggered
    return
  endif

  let g:wire#default#keybindings#triggered = 1

  for s:i in range(1, 9)
    execute 'nnoremap <silent> <leader>' . s:i . ' :' . s:i . 'wincmd w<CR>'
    call s:_reg(string(s:i), 'Window ' . s:i)
  endfor


  " GROUP A {{{

  call s:_regn('a', '+ag')
  call s:_reg('ag', ['Ag!', 'ag'])

  " }}}

  " GROUP C {{{

  " keybindings from nerdcomment
  call s:_regn('c', '+comment')

  " }}}


  " GROUP E {{{

  call s:_reg('e', ['Leaderf file', 'edit(pwd)'])

  " }}}


  " GROUP F {{{

  call s:_regn('f', '+switch/fzf')

  nnoremap <silent> <leader>fs :update<CR>
  nnoremap <silent> <leader>ff :Leaderf file ~<CR>
  call s:_reg('ft', ['NERDTreeToggle', 'toggle-filex-explorer'])
  call s:_reg('fs', 'save')
  call s:_reg('ff', 'edit(~)')

  " }}}

  " GROUP G {{{

  call s:_regn('g', '+lsp')

  "

  " GROUP J {{{

  call s:_regn('j', '+jump')
  nnoremap <silent> <leader>jb :call feedkeys("\<C-O>")<CR>
  nnoremap <silent> <leader>jf :call feedkeys("\<C-I>")<CR>
  call s:_reg('jb', 'jump-back')
  call s:_reg('jf', 'jump-forward')

  call wire#vim#keybinding#reg_local_leader_name('j', 'easymotion')
  call wire#vim#keybinding#reg_local_leader('jb', ['<Plug>(easymotion-b)', 'word-backward'])
  call wire#vim#keybinding#reg_local_leader('jw', ['<Plug>(easymotion-w)', 'word-forward'])

  call wire#vim#keybinding#reg_local_leader('jj', ['<Plug>(easymotion-j)', 'line-forward'])
  call wire#vim#keybinding#reg_local_leader('jk', ['<Plug>(easymotion-k)', 'line-backward'])

  " }}}


  " GROUP M {{{

  call s:_regn('m', '+make')

  " }}}

  " GROUP N {{{

  call s:_regn('n', '+no')
  nnoremap <silent> <leader>nl :nohl<CR>
  call s:_reg('nl', 'no-highlight')

  " }}}


  " GROUP Q {{{

  nnoremap <silent> <leader>q :q<CR>
  nnoremap <silent> <leader>Q :qa!<CR>
  call s:_reg('q', 'quit')
  call s:_reg('Q', 'force-quit')

  " }}}

  " GROUP S {{{

  call s:_regn('s', '+search/session')
  nnoremap <silent> <leader>ss :call wire#default#session#save_session()<CR>
  nnoremap <silent> <leader>sr :call wire#default#session#restore_session()<CR>
  call s:_reg('ss', 'save-session')
  call s:_reg('sr', 'restore-session')

  " }}}



  " GROUP T {{{

  call s:_regn('t', '+toggle/tabular')
  nnoremap <silent> <leader>tq :call wire#window#toggle_quickfix()<CR>
  call s:_reg('tq', 'toggle-quickfix')

  " }}}

  " GROUP V {{{

  call s:_regn('v', '+vcs')
  function! s:display_last_commit_of_current_line() abort
    let line = line('.')
    let file = expand('%')
    let cmd = 'git log -L ' . line . ',' . line . ':' . file
    let cmd .= ' --pretty=format:""%s"" -1'
    let title = systemlist(cmd)[0]
    if v:shell_error == 0
      echo 'Last commit of current line is: ' . title
    endif
  endfunction
  call s:_reg('vm', [ 'call call(' . string(function('s:display_last_commit_of_current_line')) . ', [])', 'last-commit'])

  " }}}

  " group w {
  nnoremap <Plug>(window_w) <C-W>w
  nnoremap <Plug>(window_r) <C-W>r
  nnoremap <Plug>(window_d) <C-W>c
  nnoremap <Plug>(window_q) <C-W>q
  nnoremap <Plug>(window_j) <C-W>j
  nnoremap <Plug>(window_k) <C-W>k
  nnoremap <Plug>(window_h) <C-W>h
  nnoremap <Plug>(window_l) <C-W>l
  nnoremap <Plug>(window_H) <C-W>5<
  nnoremap <Plug>(window_L) <C-W>5>
  nnoremap <Plug>(window_J) :resize +5<CR>
  nnoremap <Plug>(window_K) :resize -5<CR>
  nnoremap <Plug>(window_b) <C-W>=
  nnoremap <Plug>(window_s1) <C-W>s
  nnoremap <Plug>(window_s2) <C-W>s
  nnoremap <Plug>(window_v1) <C-W>v
  nnoremap <Plug>(window_v2) <C-W>v
  nnoremap <Plug>(window_2) <C-W>v

  call s:_regn('w', '+windows')
  call s:_reg('wv', ['call feedkeys("\<Plug>(window_v1)")', 'split-window-below'])
  call s:_reg('wm', ['call wire#window#mark()', 'mark-window'])
  call s:_reg('wr', ['call wire#window#swap()', 'do-swap'])
  call s:_reg('ww', ['call feedkeys("\<Plug>(window_w)")', 'other-window'])
  call s:_reg('wd', ['call feedkeys("\<Plug>(window_d)")', 'delete-window'])
  call s:_reg('w-', ['call feedkeys("\<Plug>(window_s1)")', 'split-window-below'])
  call s:_reg('w|', ['call feedkeys("\<Plug>(window_v1)")', 'split-window-right'])
  call s:_reg('w2', ['call feedkeys("\<Plug>(window_v1)")', 'layout-double-columns'])
  call s:_reg('wh', ['call feedkeys("\<Plug>(window_h)")', 'window-left'])
  call s:_reg('wj', ['call feedkeys("\<Plug>(window_j)")', 'window-below'])
  call s:_reg('wl', ['call feedkeys("\<Plug>(window_l)")', 'window-right'])
  call s:_reg('wk', ['call feedkeys("\<Plug>(window_k)")', 'window-up'])
  call s:_reg('wH', ['call feedkeys("\<Plug>(window_H)")', 'expand-window-left'])
  call s:_reg('wJ', ['call feedkeys("\<Plug>(window_J)")', 'expand-window-below'])
  call s:_reg('wL', ['call feedkeys("\<Plug>(window_L)")', 'expand-window-right'])
  call s:_reg('wK', ['call feedkeys("\<Plug>(window_K)")', 'expand-window-up'])
  call s:_reg('w=', ['call feedkeys("\<Plug>(window_b)")', 'balance-window'])
  call s:_reg('ws', ['call feedkeys("\<Plug>(window_s1)")', 'split-window-below'])
  call s:_reg('w?', ['Windows', 'fzf-window'])

  " GROUP X {

  nnoremap <leader>x :wq<CR>
  call s:_reg('x', 'save-and-quit')

  " }


  " other keybindins {{{

  inoremap jk <Esc>

  " }}}
endfunction
