function! s:save_session_impl(input) abort
  let vim_path = $HOME . '/.vim/session'
  let session_path = vim_path . '/' . a:input . '.vim'

  if !isdirectory(vim_path)
    call mkdir(vim_path, 'p')
  endif
  execute 'mksession! ' . session_path
endfunction

function! wire#default#session#save_session() abort
  let input = input('Session file: ')
  call s:save_session_impl(input)
endfunction

function! wire#default#session#restore_session() abort
  let input = input('Session file: ')
  let vim_path = $HOME . '/.vim/session'
  let session_path = vim_path . '/' . input . '.vim'
  execute 'so ' . session_path
  if bufexists(1)
    for l in range(1, bufnr('$'))
      if bufwinnr(l) == -1
        exec 'sbuffer ' . l
      endif
    endfor
  endif
endfunction

