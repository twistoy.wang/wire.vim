let s:crate_tab = get(s:, 'crate_tab', -1)
let s:crate_buf = get(s:, 'crate_buf', -1)

let s:logger = wire#vim#log#get_logger('wire.crate')

let s:crate_root = g:wire.base . '/crate'

function! wire#crate#has_loaded(crate) abort "{{{
  return index(g:wire.loaded_crates, a:crate) > -1 ? 1 : 0
endfunction "}}}

function! wire#crate#initialize(root_dir) abort "{{{
  let l:crate_category_dirs = filter(split(globpath(a:root_dir, '*'), '\n'),
        \ 'isdirectory(v:val)')

  let l:crates_category = deepcopy(l:crate_category_dirs)

  for l:category in l:crates_category
    let l:crate_full_dir = split(globpath(l:category, '*'), '\n')
    let l:crate_dirs = deepcopy(l:crate_full_dir)
    let l:crate_names = map(l:crate_dirs, 'fnamemodify(v:val, ":t")')
    let l:category_name = fnamemodify(l:category, ':t')

    call s:logger.info('Found crates ' . string(l:crate_names) . ' at ' . string(l:category))

    for l:crate in l:crate_full_dir
      let l:k = fnamemodify(l:crate, ':t')
      let g:available_crates[l:k] = { 'dir': l:crate }
    endfor
  endfor
endfunction "}}}

function! wire#crate#load_configs() abort "{{{
  for l:crate in g:wire.loaded_crates
    if has_key(g:available_crates, l:crate)
      call wire#utils#source(g:available_crates[l:crate].dir . '/cfg.vim')
    else
      call s:logger.warn('Crate "' . l:crate . '" not found.')
    endif
  endfor

  if exists('g:private')
    for l:crate in g:private
      let l:create_cfg_file = g:wire.base . '/private/' . l:crate . '/cfg.vim'
      call wire#utils#source(l:create_cfg_file)
    endfor
  endif

  call wire#utils#source(g:wire.base . '/private/cfg.vim')
endfunction "}}}

