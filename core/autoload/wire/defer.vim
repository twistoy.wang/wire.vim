function! s:defer_load(...)
  for l:plug in a:000
    silent! call plug#load(l:plug)
  endfor
endfunction

function! wire#defer#leaderf(timer) abort
  call s:defer_load('LeaderF')
endfunction

function! wire#defer#whichkey(timer) abort
  call wire#vim#keybinding#build()
endfunction
