scriptencoding utf-8

let g:wire.loaded_crates = ['wire']
let g:wire.excluded = []
let g:wire.plugins = []
let g:wire.plugin_options = {}

let g:available_crates = {}

let s:custom_file = $HOME.'/.wire'
let s:logger = wire#vim#log#get_logger('wire.main')

function! wire#bootstrap() abort "{{{
  call wire#begin()
  call wire#end()
endfunction "}}}

function! wire#begin() abort "{{{
  call wire#utils#check_vim_plugin()
  call s:defined_command()

  " initialize crate before all
  call wire#crate#initialize(g:wire.crate_root)

  let g:mapleader = "\<Space>"
  let g:maplocalleader = ','

  call s:read_custom_file()
endfunction " }}}

function! wire#end() abort "{{{
  call wire#vim#plug#install_all()

  " install missing plugins, with ask
  " FIXME(hawtian): try to defer this
  " call wire#vim#plug#install_missing(1)

  call wire#crate#load_configs()

  if exists('*UserConfig')
    call UserConfig()
  endif
endfunction "}}}

function! s:defined_command() abort "{{{
  command! -nargs=+ -bar UsePlug call s:register_plugin(<args>)
endfunction "}}}

function! s:read_custom_file() abort "{{{
  if filereadable(expand(s:custom_file))
    call wire#utils#source(s:custom_file)

    if exists('g:wire_enable_crates')
      let g:wire.loaded_crates = g:wire.loaded_crates + g:wire_enable_crates
    endif
  endif
endfunction "}}}

function! s:register_plugin(plug, ...) abort "{{{
  if index(g:wire.plugins, a:plug) < 0 "{
    call s:logger.info('Add Plugin "' . a:plug . '" success.')
    call add(g:wire.plugins, a:plug)
  endif "}

  " if there's configuration
  if a:0 == 1 "{
    " simple save plugin configurations
    let g:wire.plugin_options[a:plug] = a:1
  endif "}
endfunction "}}}

function! s:to_array(value) abort "{{{
  if type(a:value) == wire#utils#TYPE.list "{
    return a:value
  else
    return [a:value]
  endif "}
endfunction "}}}

